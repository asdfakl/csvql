# csvql

### A Clojure csv query utility

#### Installation

```shell
git clone git@gitlab.com:asdfakl/csvql.git
cd csvql
lein uberjar
```

#### Usage

```shell
java -jar target/uberjar/csvql-0.1.0-SNAPSHOT-standalone.jar /path/to/file.csv 'SELECT :*'
```

Expects input file formatted according to https://tools.ietf.org/html/rfc4180

Supports LF / CRLF line endings.

First row is considered header row (headless operation is on the todo-list).

#### SELECT

```SQL
SELECT :*

SELECT :Order_ID :Order_Date :Item_Type :Units_Sold
```

All select expressions must be prefixed with a colon. 

Spaces in header fields are replaced with underscores: `"Order Date" -> "Order_Date"`

#### SELECT DISTINCT

```SQL
SELECT DISTINCT :Region :Country
```

Use DISTINCT to only print each distinct result once.

Results are distinct if at least one of their fields differ.

The asterisk selector **:*** cannot be used with DISTINCT.

#### SEPARATOR

```SQL
SEPARATOR \; SELECT :Region :Country
```

Default separator comma, can be changed with `SEPARATOR` expression.

#### FILTER

```SQL
SELECT :* FILTER :Region = "Asia" AND :Total_Profit > 1000000

SELECT :* FILTER :Order_ID = 518511188

SELECT :* FILTER :Country = "Finland" OR :Country = "Estonia"

SELECT :* FILTER :Country = "Finland" AND :Units_Sold < 1000 OR :Country = "Estonia" AND :Units_Sold > 5000

SELECT :* FILTER (:Country = "Finland" OR :Country = "Estonia") AND (:Item_Type = "Fruits" OR :Item_Type = "Cereal" OR :Item_Type = "Meat")

SELECT DISTINCT :Country FILTER :Country LIKE ".*can.*"

SELECT DISTINCT :Item_Type FILTER :Item_Type ILIKE ".*suppl.*"
```

Supported comparison operators are: **= < > LIKE ILIKE**

In expressions where rhs is string:

* **=** matches field value explicitly (case sensitive)
* **LIKE** matches field values matching given regular expression (https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html)
* **ILIKE** matches field values matching given case-insensitive regular expression (wrapper for `(?i)` flag)

For numeric **equality** both lhs and rhs are cast to integers before comparison. If the field value is not numeric, the row does not match.

For numeric **lt** and **gt** comparisons floating-point field values are used. Non-numeric field values don't match.

#### LIMIT and OFFSET

```SQL
SELECT :* FILTER :foo = "bar" LIMIT 1000 OFFSET 50000
```

Limit result set to `LIMIT` entries beginning from offset `OFFSET`.


#### Acknowledgements

Sample csv files: http://eforexcel.com/wp/

Clojure csv reader/writer: https://github.com/clojure/data.csv
