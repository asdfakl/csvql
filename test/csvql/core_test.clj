(ns csvql.core-test
	(:require [clojure.test :refer :all]
		[csvql.core :refer :all]))

(defmacro test-query
	[query data headers & asserts]
	`(testing ~query
		(let [~'query (parse-query ~query) ~'results (do-query ~data ~'query ~headers)]
			~@asserts)))

(def animal-headers [:species :name :age])
(def animal-lines '(
	["dog" "Woofy" "4"]
	["dog" "Barky" "8"]
	["turtle" "Raphaello"]
	["cat" "Kitty" "10"]
	["dog" "Spotty" "4"]
	["turtle" "Michelangelo"]))
(def animals (parse-lines animal-lines animal-headers))

(deftest select-tests
	(test-query
		"SELECT :name :species"
		animals
		animal-headers
		(is (= (count results) 6))
		(is (= (first results) '("Woofy" "dog")))
		(is (= (second results) '("Barky" "dog")))
		(is (= (nth results 2) '("Raphaello" "turtle"))))
	(test-query
		"SELECT :*"
		animals
		animal-headers
		(is (= (count results) 6))
		(is (= (first results) '("dog" "Woofy" "4")))
		(is (= (second results) '("dog" "Barky" "8")))
		(is (= (nth results 2) '("turtle" "Raphaello" nil))))
	(test-query
		"SELECT DISTINCT :species"
		animals
		animal-headers
		(is (= (count results) 3))
		(is (= (set results) #{'("dog") '("cat") '("turtle")})))
	(test-query
		"SELECT DISTINCT :age :species"
		animals
		animal-headers
		(is (= (count results) 4))
		(is (= (set results) #{'("10" "cat") '(nil "turtle") '("4" "dog") '("8" "dog")}))))

(deftest select-index-tests
	(test-query
		"SELECT 2 1 0"
		animals
		animal-headers
		(is (= (count results) 6))
		(is (= (first results) '("4" "Woofy" "dog")))
		(is (= (last results) '(nil "Michelangelo" "turtle"))))
	(test-query
		"SELECT DISTINCT 0"
		animals
		animal-headers
		(is (= (count results) 3))
		(is (= (set results) #{'("turtle") '("dog") '("cat")}))))

(deftest filter-tests
	(test-query
		"SELECT :name FILTER :age > 5"
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Barky")))
		(is (= (second results) '("Kitty"))))
	(test-query
		"SELECT :name FILTER :age < 5"
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Woofy")))
		(is (= (second results) '("Spotty"))))
	(test-query
		"SELECT :name FILTER :age = 5"
		animals
		animal-headers
		(is (empty? results)))
	(test-query
		"SELECT :name FILTER :age = 8"
		animals
		animal-headers
		(is (= (count results) 1))
		(is (= (first results) '("Barky"))))
	(test-query
		"SELECT :name FILTER :species = \"turtle\""
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Raphaello")))
		(is (= (second results) '("Michelangelo"))))
	(test-query
		"SELECT :name FILTER (:species = \"cat\" OR :species = \"dog\") AND (:age > 5 OR :name LIKE \".*lo\")"
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Barky")))
		(is (= (second results) '("Kitty"))))
	(test-query
		"SELECT :name FILTER :species LIKE \".*urt.*\""
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Raphaello")))
		(is (= (second results) '("Michelangelo"))))
	(test-query
		"SELECT :name FILTER :species LIKE \".*URT.*\""
		animals
		animal-headers
		(is (empty? results)))
	(test-query
		"SELECT :name :species FILTER :name ILIKE \".*TTY\""
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Kitty" "cat")))
		(is (= (second results) '("Spotty" "dog")))))

(deftest filter-index-tests
	(test-query
		"SELECT :name FILTER 0 = \"cat\""
		animals
		animal-headers
		(is (= (count results) 1))
		(is (= (first results) '("Kitty"))))
	(test-query
		"SELECT :name FILTER 2 < 5"
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("Woofy")))
		(is (= (second results) '("Spotty"))))
	(test-query
		"SELECT 0 1 FILTER 0 = \"cat\" OR 2 = 8" ;; ok this just looks wrong, TODO maybe use @n syntax?
		animals
		animal-headers
		(is (= (count results) 2))
		(is (= (first results) '("dog" "Barky")))
		(is (= (second results) '("cat" "Kitty")))))

(def number-headers [:number :oddity])
(def numbers
	(let [oddity {true "odd" false "even"}]
		(map #(hash-map :number (str %) :oddity (get oddity (odd? %))) (map inc (range 100)))))

(deftest limit-tests
	(test-query
		"SELECT :number LIMIT 0"
		numbers
		number-headers
		(is (empty? results)))
	(test-query
		"SELECT :number LIMIT 1"
		numbers
		number-headers
		(is (= (count results) 1))
		(is (= (first results) '("1"))))
	(test-query
		"SELECT :number FILTER :oddity = \"even\" LIMIT 10"
		numbers
		number-headers
		(is (= (count results) 10))
		(is (= (first results) '("2")))
		(is (= (last results) '("20"))))
	(test-query
		"SELECT :number FILTER :oddity = \"none\" LIMIT 25"
		numbers
		number-headers
		(is (= (count results) 0)))
	(test-query
		"SELECT :number FILTER :number > 90 LIMIT 9"
		numbers
		number-headers
		(is (= (count results) 9))
		(is (= (first results) '("91")))
		(is (= (last results) '("99"))))
	(test-query
		"SELECT :number FILTER :number > 50 LIMIT 50"
		numbers
		number-headers
		(is (= (count results) 50))
		(is (= (first results) '("51")))
		(is (= (last results) '("100"))))
	(test-query
		"SELECT :number LIMIT 101"
		numbers
		number-headers
		(is (= (count results) 100))
		(is (= (first results) '("1")))
		(is (= (last results) '("100"))))
	(test-query
		"SELECT :number LIMIT 100000000"
		numbers
		number-headers
		(is (= (count results) 100))
		(is (= (first results) '("1")))
		(is (= (last results) '("100"))))
	(test-query
		"SELECT :number FILTER :oddity = \"odd\" LIMIT 51"
		numbers
		number-headers
		(is (= (count results) 50))
		(is (= (first results) '("1")))
		(is (= (last results) '("99"))))
	(test-query
		"SELECT :number FILTER :oddity = \"even\" LIMIT 99"
		numbers
		number-headers
		(is (= (count results) 50))
		(is (= (first results) '("2")))
		(is (= (last results) '("100")))))

(deftest offset-tests
	(test-query
		"SELECT :number OFFSET 0"
		numbers
		number-headers
		(is (= (count results) 100))
		(is (= (first results) '("1"))))
	(test-query
		"SELECT :number OFFSET 1"
		numbers
		number-headers
		(is (= (count results) 99))
		(is (= (first results) '("2"))))
	(test-query
		"SELECT :number FILTER :oddity = \"even\" OFFSET 25"
		numbers
		number-headers
		(is (= (count results) 25))
		(is (= (first results) '("52"))))
	(test-query
		"SELECT :number FILTER :oddity = \"odd\" OFFSET 40"
		numbers
		number-headers
		(is (= (count results) 10))
		(is (= (first results) '("81"))))
	(test-query
		"SELECT :number OFFSET 99"
		numbers
		number-headers
		(is (= (count results) 1))
		(is (= (first results) '("100"))))
	(test-query
		"SELECT :number OFFSET 100"
		numbers
		number-headers
		(is (empty? results)))
	(test-query
		"SELECT :number OFFSET 253453"
		numbers
		number-headers
		(is (empty? results))))

(deftest limit-offset-tests
	(test-query
		"SELECT :number LIMIT 10 OFFSET 10"
		numbers
		number-headers
		(is (= (count results) 10))
		(is (= (first results) '("11")))
		(is (= (last results) '("20"))))
	(test-query
		"SELECT :number FILTER :oddity = \"even\" LIMIT 10 OFFSET 10"
		numbers
		number-headers
		(is (= (count results) 10))
		(is (= (first results) '("22")))
		(is (= (last results) '("40"))))
	(test-query
		"SELECT :number LIMIT 10 OFFSET 90"
		numbers
		number-headers
		(is (= (count results) 10))
		(is (= (first results) '("91")))
		(is (= (last results) '("100"))))
	(test-query
		"SELECT :number LIMIT 10 OFFSET 95"
		numbers
		number-headers
		(is (= (count results) 5))
		(is (= (first results) '("96")))
		(is (= (last results) '("100"))))
	(test-query
		"SELECT :number LIMIT 10 OFFSET 100"
		numbers
		number-headers
		(is (empty? results)))
	(test-query
		"SELECT :number LIMIT 10 OFFSET 231355"
		numbers
		number-headers
		(is (empty? results))))
