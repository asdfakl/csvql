(ns csvql.filter)

(declare filter-exp
	make-function
	parse-filter-exp
	non-neg-int?
	lhs-exp
	append-and
	append-or)

(def long-expression #"\A\d+\z")
(def float-expression #"\A-?\d+(\.\d+)?\z")

(defn parse-filter
	[tokens]
	(make-function (parse-filter-exp tokens)))

(defn- parse-filter-exp
	([tokens]
		(parse-filter-exp tokens [] '(or (and))))
	([tokens buf res]
		(if (empty? tokens)
			(append-and res (apply filter-exp buf))
			(let [[token & tail] tokens]
				(case token
					AND (parse-filter-exp tail [] (append-and res (apply filter-exp buf)))
					OR (parse-filter-exp tail [] (append-or (append-and res (apply filter-exp buf))))
					(parse-filter-exp tail (conj buf token) res))))))

(defn make-function
	[tpl]
	(eval `(fn [~'row] ~tpl)))

(defn string-number-eq
	"Compare string to number. s matches n if it can be converted to float and its long-value matches the long-value of n."
	[s n]
	(if (string? s)
		(if (re-matches long-expression s)
			(== (long n) (Long/parseLong s))
			(if (first (re-matches float-expression s))
				(== (long n) (long (Float/parseFloat s)))
				false))
		false))

(defn string-number-comp
	[f s n]
	(if (string? s)
		(if (first (re-matches float-expression s))
			(f (Float/parseFloat s) n)
			false)
		false))

(def string-number-lt (partial string-number-comp <))
(def string-number-gt (partial string-number-comp >))

(defn string-matches
	[re s]
	(if (string? s)
		(re-matches re s)
		false))

(defn filter-exp
	([coll]
		(if (list? coll)
			(parse-filter-exp coll)
			(throw (Exception. (str "invalid filter expression: " coll)))))
	([lhs comp-fn rhs]
		(if (not (or (keyword? lhs) (non-neg-int? lhs))) (throw (Exception. (str "lhs must be keyword or non-negative integer in expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
		(case comp-fn
			= (cond
				(string? rhs) (list comp-fn (lhs-exp lhs) rhs)
				(number? rhs) (list `string-number-eq (lhs-exp lhs) rhs)
				:else (throw (Exception. (str "invalid filter expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
			< (if (number? rhs)
				(list `string-number-lt (lhs-exp lhs) rhs)
				(throw (Exception. (str "rhs must be number in expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
			> (if (number? rhs)
				(list `string-number-gt (lhs-exp lhs) rhs)
				(throw (Exception. (str "rhs must be number in expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
			LIKE (if (string? rhs)
				(list `string-matches (re-pattern rhs) (lhs-exp lhs))
				(throw (Exception. (str "rhs must be string in expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
			ILIKE (if (string? rhs)
				(list `string-matches (re-pattern (str "(?i)" rhs)) (lhs-exp lhs))
				(throw (Exception. (str "rhs must be string in expression: " (clojure.string/join " " (list lhs comp-fn rhs))))))
			(throw (Exception. (str "unsupported comparison operator: " comp-fn))))))

(defn non-neg-int?
	[n]
	(and (integer? n) (not (neg? n))))

(defn lhs-exp
	[lhs]
	(list `get 'row lhs))

(defn append-and
	"expects expression of form (or (and) (and) ...), appends elem to last and-block"
	[exp elem]
	(concat (butlast exp) (list (concat (last exp) (list elem)))))

(defn append-or
	"expects expression of form (or (and) (and) ...), appends empty and-block"
	[exp]
	(concat exp (list '(and))))
