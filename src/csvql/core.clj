(ns csvql.core
	(:require [clojure.java.io :as io]
		[clojure.data.csv :as csv]
		[csvql.filter :refer [parse-filter]]
		[csvql.select :refer :all])
	(:gen-class))

(declare 
	do-query
	parse-lines
	parse-query
	build-query
	keywordify
	transform-distinct
	transform-limit)

(defn -main
	[& args]
	(if (not (= (count args) 2)) (throw (Exception. "Usage: [FILENAME] [QUERY]")))
	(let [[filename query] args]
		(with-open [reader (io/reader filename)]
			(let [query (parse-query query)
				lines (csv/read-csv reader :separator (:separator query))
				headers (map keywordify (first lines))]
				(csv/write-csv *out* (list (format-selection (:select query) headers)) :separator (:separator query))
				(csv/write-csv *out* (do-query (parse-lines (next lines) headers) query headers) :separator (:separator query))))
		(flush)))

(defn parse-lines
	[lines headers]
		(lazy-seq
			(when-let [ss (seq lines)]
				(cons (merge (zipmap (range) (first lines)) (zipmap headers (first lines))) (parse-lines (next lines) headers)))))

(defn get-query-results
	[data query headers]
	(map (partial select-row (:select query) headers) (filter (:filter query) data)))

(defn do-query
	[data query headers]
	(let [t-distinct (transform-distinct query)
		t-limit (transform-limit query)
		t-offset (partial drop (:offset query))]
		(-> (get-query-results data query headers)
			t-offset
			t-limit
			t-distinct)))

(defn parse-query
	([query] (parse-query (read-string (str "(" query ")")) :initial {}))
	([query state res]
		(if (empty? query)
			(build-query res)
			(let [[token & remaining] query]
				(case state
					:initial (case token
						SEPARATOR (parse-query remaining :separator res)
						SELECT (parse-query remaining :select (assoc res :select-buf [])))
					:separator (if (char? token)
						(parse-query remaining :initial (assoc res :separator token))
						(throw (Exception. (str "invalid separator " token))))
					:select (case token
						DISTINCT (parse-query remaining :select (assoc res :distinct true))
						FILTER (parse-query remaining :filter (assoc res :filter-buf []))
						LIMIT (parse-query remaining :limit res)
						OFFSET (parse-query remaining :offset res)
						(parse-query remaining state (assoc res :select-buf (conj (:select-buf res) token))))
					:filter (case token
						LIMIT (parse-query remaining :limit res)
						OFFSET (parse-query remaining :offset res)
						(parse-query remaining state (assoc res :filter-buf (conj (:filter-buf res) token))))
					:limit (if (and (integer? token) (not (neg? token)))
						(parse-query remaining :after-limit (assoc res :limit token))
						(throw (Exception. (str "invalid limit " token))))
					:after-limit (case token
						OFFSET (parse-query remaining :offset res))
					:offset (if (and (integer? token) (not (neg? token)))
						(parse-query remaining :done (assoc res :offset token))
						(throw (Exception. (str "invalid offset " token)))))))))

(defn build-query
	[query]
	{ :select (parse-select (:select-buf query))
		:filter (if (:filter-buf query)
			(parse-filter (reverse (into '() (:filter-buf query))))
			identity)
		:distinct (:distinct query)
		:separator (or (:separator query) \,)
		:limit (:limit query)
		:offset (or (:offset query) 0)})

(defn keywordify
	[s]
	(keyword (clojure.string/replace s #"\s" "_")))

(defn transform-distinct
	[query]
	(if (and (:distinct query) (not (vector? (:select query)))) (throw (Exception. (str "cannot select distinct from selection: " (:select query)))))
	(if (:distinct query)
		distinct
		identity))

(defn transform-limit
	[query]
	(if (:limit query)
		(partial take (:limit query))
		identity))
