(ns csvql.select
	(:require [csvql.filter :refer [non-neg-int?]]))

(defn parse-select
	[filter-buffer]
	(if (empty? filter-buffer) (throw (Exception. "empty select statement")))
	(if (and (= (count filter-buffer) 1) (= (first filter-buffer) :*))
		:all
		(loop [buf filter-buffer result []]
			(if (empty? buf)
				result
				(let [[token & remaining] buf]
					(if (and (or (keyword? token) (non-neg-int? token)) (not (= token :*)))
						(recur remaining (conj result token))
						(throw (Exception. (str "invalid select expression: " (clojure.string/join " " filter-buffer))))))))))

(defn select-row
	[select header-row row]
	(if (not (map? row))
		(throw (IllegalArgumentException. (str (type row) " passed to csvql.select/select-row")))
		(if (= :all select)
			(map #(get row %) header-row)
			(map #(get row %) select))))

(defn format-selection
	[select header-row]
	(if (= select :all)
		(map #(clojure.string/replace (str %) #"^:" "") header-row)
		(map #(clojure.string/replace (str %) #"^:" "") select)))
